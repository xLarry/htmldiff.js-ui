import React, { Component, useCallback } from 'react'
import Interweave from 'interweave'

import FileDrop from './components/filedrop'

import diff from './lib/htmldiff'

import './App.css';

class App extends Component {
  
  state = {
    fileContents: {
      A: '',
      B: ''
    },
    diffResult: ''
  }
  
  componentDidMount = () => {
    
  }
  
  handleFileDrop = identifier => 
    droppedFiles => {
      const reader = new FileReader()

      reader.onabort = () => console.log('file reading was aborted')
      reader.onerror = () => console.log('file reading has failed')
      reader.onload = () => {
        // Do whatever you want with the file contents
        const binaryStr = reader.result
        
        let {fileContents} = this.state
        fileContents[identifier] = binaryStr
        
        this.setState({ fileContents })
      }

      droppedFiles.forEach(file => reader.readAsBinaryString(file))
    }
  
  render() {
    const { fileContents, diffResult } = this.state
    
    return (
      <div>
        <div id="dropWrapper">
        
          {fileContents['A'].length > 0 ?
            <section>
              <div class="filecontent">
                <Interweave content={ fileContents['A'] } />
              </div>
            </section>
            : <FileDrop onDrop={this.handleFileDrop('A')} />
          }
          
          {fileContents['B'].length > 0 ?
            <section>
              <div class="filecontent">
                <Interweave content={ fileContents['B'] } />
              </div>
            </section>
            : <FileDrop onDrop={this.handleFileDrop('B')} />
          }
          
        </div>
        { fileContents.A.length > 0 && fileContents.B.length > 0 ? (
          <div id="diffResult">
            <Interweave content={ diff(fileContents['A'], fileContents['B']) } />
          </div>
        ) : null }
      </div>
    );
  }
}

export default App;

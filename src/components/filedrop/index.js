import React from 'react'
import Dropzone from 'react-dropzone'

class FileDrop extends React.Component {
	
	render = () => {
		return (
			<Dropzone onDrop={this.props.onDrop}>
				{({getRootProps, getInputProps}) => (
					<section>
						<div {...getRootProps()}>
							<input {...getInputProps()} />
							<p>Drop file or click to select manually.</p>
						</div>
					</section>
				)}
			</Dropzone>
		)
	}
}

export default FileDrop